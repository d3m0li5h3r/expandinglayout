package com.d3m0li5h3r.widgets.expandinglistview;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * A custom listview which supports the preview of extra content corresponding
 * to each cell by clicking on the cell to hide and show the extra content.
 */
public class ExpandingListView extends ListView {
    private List<View> mViewsToDraw = new ArrayList<View>();

    public ExpandingListView(Context context) {
        super(context);
    }

    public ExpandingListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandingListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * This method expands the view that was clicked and animates all the views
     * around it to make room for the expanding view. There are several steps
     * required to do this which are outlined below.
     * <p/>
     * 1. Store the current top and bottom bounds of each visible item in the
     * listview. 2. Update the layout parameters of the selected view. In the
     * context of this method, the view should be originally collapsed and set
     * to some custom height. The layout parameters are updated so as to wrap
     * the content of the additional text that is to be displayed.
     * <p/>
     * After invoking a layout to take place, the listview will order all the
     * items such that there is space for each view. This layout will be
     * independent of what the bounds of the items were prior to the layout so
     * two pre-draw passes will be made. This is necessary because after the
     * layout takes place, some views that were visible before the layout may
     * now be off bounds but a reference to these views is required so the
     * animation completes as intended.
     * <p/>
     * 3. The first predraw pass will set the bounds of all the visible items to
     * their original location before the layout took place and then force
     * another layout. Since the bounds of the cells cannot be set directly, the
     * method setSelectionFromTop can be used to achieve a very similar effect.
     * 4. The expanding view's bounds are animated to what the final values
     * should be from the original bounds. 5. The bounds above the expanding
     * view are animated upwards while the bounds below the expanding view are
     * animated downwards. 6. The extra text is faded in as its contents become
     * visible throughout the animation process.
     * <p/>
     * It is important to note that the listview is disabled during the
     * animation because the scrolling behaviour is unpredictable if the bounds
     * of the items within the listview are not constant during the scroll.
     */

    public void expandView(final View view) {
        final ExpandableItemObject viewObject = (ExpandableItemObject) getItemAtPosition(getPositionForView(view));

        final int oldBottom = view.getBottom();

		/* Update the layout so the extra content becomes visible. */
        final View expandingLayout = view.findViewById(R.id.expanding_layout);
        expandingLayout.setVisibility(View.VISIBLE);

        final ViewTreeObserver observer = getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                int newBottom = view.getBottom();

                ObjectAnimator.ofInt(view, "bottom", oldBottom, newBottom)
                        .setDuration(150).start();

                observer.removeOnPreDrawListener(this);
                return false;
            }
        });
        viewObject.setExpanded(true);
    }

    /**
     * By overriding dispatchDraw, we can draw the cells that disappear during
     * the expansion process. When the cell expands, some items below or above
     * the expanding cell may be moved off screen and are thus no longer
     * children of the ListView's layout. By storing a reference to these views
     * prior to the layout, and guaranteeing that these cells do not get
     * recycled, the cells can be drawn directly onto the canvas during the
     * animation process. After the animation completes, the references to the
     * extra views can then be discarded.
     */
    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if (mViewsToDraw.size() == 0) {
            return;
        }

        for (View v : mViewsToDraw) {
            canvas.translate(0, v.getTop());
            v.draw(canvas);
            canvas.translate(0, -v.getTop());
        }
    }

    /**
     * This method collapses the view that was clicked and animates all the
     * views around it to close around the collapsing view. There are several
     * steps required to do this which are outlined below.
     * <p/>
     * 1. Update the layout parameters of the view clicked so as to minimize its
     * height to the original collapsed (default) state. 2. After invoking a
     * layout, the listview will shift all the cells so as to display them most
     * efficiently. Therefore, during the first predraw pass, the listview must
     * be offset by some amount such that given the custom bound change upon
     * collapse, all the cells that need to be on the screen after the layout
     * are rendered by the listview. 3. On the second predraw pass, all the
     * items are first returned to their original location (before the first
     * layout). 4. The collapsing view's bounds are animated to what the final
     * values should be. 5. The bounds above the collapsing view are animated
     * downwards while the bounds below the collapsing view are animated
     * upwards. 6. The extra text is faded out as its contents become visible
     * throughout the animation process.
     */

    public void collapseView(final View view) {
        final ExpandableItemObject viewObject = (ExpandableItemObject) getItemAtPosition(getPositionForView(view));

        final int oldBottom = view.getBottom();

        View expandingLayout = view.findViewById(R.id.expanding_layout);
        expandingLayout.setVisibility(View.GONE);
        view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT));
        viewObject.setExpanded(false);

        final ViewTreeObserver observer = getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                int newBottom = view.getBottom();

                ObjectAnimator.ofInt(view, "bottom", oldBottom, newBottom)
                        .setDuration(150).start();

                observer.removeOnPreDrawListener(this);
                return false;
            }
        });
    }
}
