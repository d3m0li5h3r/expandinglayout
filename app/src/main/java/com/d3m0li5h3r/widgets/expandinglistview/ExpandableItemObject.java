package com.d3m0li5h3r.widgets.expandinglistview;
/**
 * Created by d3m0li5h3r on 9/10/14.
 */
public class ExpandableItemObject  implements OnSizeChangedListener {
    protected boolean mIsExpanded = false;
    protected int mExpandedHeight = -1;

    public boolean isExpanded() {
        return mIsExpanded;
    }

    public int getExpandedHeight() {
        return mExpandedHeight;
    }

    public void setExpanded(boolean isExpanded) {
        mIsExpanded = isExpanded;
    }

    public void setExpandedHeight(int expandedHeight) {
        mExpandedHeight = expandedHeight;
    }
    @Override
    public void onSizeChanged(int newHeight) {
        setExpandedHeight(newHeight);
    }
}
