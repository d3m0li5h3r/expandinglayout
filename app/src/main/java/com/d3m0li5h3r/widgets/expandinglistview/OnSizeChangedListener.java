package com.d3m0li5h3r.widgets.expandinglistview;
/**
 * A listener used to update the list data object when the corresponding expanding
 * layout experiences a size change.
 */
public interface OnSizeChangedListener {
    public void onSizeChanged(int newHeight);
}
